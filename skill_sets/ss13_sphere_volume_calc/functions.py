#!/usr/bin/env python3

# Developer: Elka Anistratenko
# Course: LIS4369
# Semester: Fall 2021

import math

def get_requirements():
	print("\nSphere Volume Program")
	print("Developer: Elka Anistratenko")
	print("\nProgram Requirements:\n"
		+ "1. Program calculates sphere volume in liquid U.S. gallons from user-entered diameter value in inches, and rounds to two decimal places.\n"
        + "2. Must use Python's *built-in* PI and pow() capabilities.\n"
        + "3. Program checks for non-integers and non-numeric values.\n"
        + "4. Program continues to promptfor user entry until no longer requested, prompt accepts upper or lower case letters.\n")

def sphere_volume():
    # initialize variables
    diameter = 0
    volume = 0.0
    gallons = 0.0
    choice = ' ' # initialize to space character

    # IPO: Input > Process > Output
    # get user data
    print("Input: ")

    choice = input("Do you want to calculate a sphere volume (y or n)? ").lower()
    
    # Process and Output:
    print("\nOutput: ")

    """ TypeError vs ValueError:
    Passing arguments of wrong type (e.g., passing a list when an int is expected) results in a TypeError, 
    but, passing arguments with wrong value (e.g., a number outside expected boundaries) results in a ValueError.
    Below, ValueError exception testing is used to test for *any* non-integer value.
    """

    while (choice[0] == 'y'):
        diameter = input("Please enter diameter in inches (integers only): ")
        test = True
        while (test == True):
            try:
                diameter = int(diameter) # test for integer value
                # Below *must* use floating point vals (not int), returns cubic inches
                volume = ((4.0 / 3.0) * math.pi * math.pow(diameter / 2.0, 3))
                gallons = volume / 231 # convert cubic inches into gallons
                print("\nSphere volume: {0:,.2f} liquid U.S. gallons\n".format(gallons))
                test = False

            except ValueError:
                print("\nNot valid integer!")
                diameter = input("Please enter diameter in inches: ")
            continue

        choice = input("Do you want to calculate another sphere volume? ").lower()

    print("\nThank you for using our Sphere Volume Calculator!")