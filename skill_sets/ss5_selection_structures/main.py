#!/usr/bin/env python3

import functions as f

def main():
    f.get_requirements()
    user_input = f.get_user_input()

    # for testing purposes: print and quit (halts program execution)
    # print(user_input) # prints values
    # print(type(user_input)) # prints type
    # quit()

    # tuple unpacking: tuple values unpacked into variable names
    n1, n2, operator = user_input

    # pass user-entered values
    f.print_selection_structures(n1, n2, operator)

if __name__ == "__main__":
    main()