#!/usr/bin/env python3
# Developer: Elka Anistratenko
# Course: LIS4369
# Semester: Fall 2021

def print_requirements():
    print("Miles Per Gallon")
    print("Developer: Elka Anistratenko\n")
    print("Program Requirements:\n"
        + "1. Convert MPG.\n"
        + "2. Must use float data type for user input and calculation.\n"
        + "3. Format and round conversion to two decimal places.\n")

def calc_mpg():
    print("Input:")
    miles = float(input("Enter miles driven: "))
    gals = float(input("Enter gallons of fuel used: "))

    mpg = miles / gals

    print("\nOutput:")
    print("{0:,.2f} {1} {2:,.2f} {3} {4:,.2f} {5}".format(miles, "miles driven and", gals, "gallons used =", mpg, "mpg"))