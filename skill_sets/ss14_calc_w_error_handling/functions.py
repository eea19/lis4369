#!/usr/bin/env python3

# Developer: Elka Anistratenko
# Course: LIS4369
# Semester: Fall 2021

def get_requirements():
	print("\nPython Calculator with Error Handling")
	print("Developer: Elka Anistratenko")
	print("\nProgram Requirements:\n"
		+ "1. Program calculates two numbers and rounds to two decimal places.\n"
        + "2. Prompt user for two numbers and a suitable operator.\n"
        + "3. Use Python error handling to validate data.\n"
        + "4. Test for correct arithmetic operator.\n"
        + "5. Division by zero not permitted.\n"
        + "6. Note: Program loops until correct input entered - numbers and arithmetic operator.\n"
        + "7. Replicate display below.\n")

def getNum(prompt):
    while True:
        try:
            return float(input("\n" + prompt + " "))
        except ValueError:
            print("Not a valid number! Try again!")

def getOp():
    validOperators = ['+','-','*','/','//','%','**']
    print("\nSuitable Operators: +, -, *, /, // (integer division), % (modulo operator), ** (power): ")
    while True:
        op = input("Enter operator: ")
        try:
            validOperators.index(op) # check if user-enetered operator is in the list
            return op
        except ValueError:
            print("\nInvalid operator! Try again!")

def get_calc():
    num1 = getNum("Enter num1: ")
    num2 = getNum("Enter num2: ")
    op = getOp()
    sum = 0.0

    if op == '+':
        sum = num1 + num2
    elif op == '-':
        sum = num1 - num2
    elif op == '*':
        sum = num1 * num2
    elif op == '**':
        sum = num1 ** num2
    elif op == '/':
        while True:
            try:
                sum = num1 / num2
                break
            except ZeroDivisionError:
                num2 = getNum("Cannot divide by zero! Try again! \nEnter num2: ") # prompt for num2 again
    elif op == '//':
        while True:
            try:
                sum = num1 // num2
                break
            except ZeroDivisionError:
                num2 = getNum("Cannot divide by zero! Try again! \nEnter num2: ") # prompt for num2 again
    elif op == '%':
        while True:
            try:
                sum = num1 % num2
                break
            except ZeroDivisionError:
                num2 = getNum("Cannot divide by zero! Try again! \nEnter num2: ") # prompt for num2 again
    
    print("\nAnswer is " + str(round(sum,2)))
    print("\nThank you for using our Math Calculator!")