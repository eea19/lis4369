#!/usr/bin/env python3
import functions as f

def main():
	f.get_requirements()
	# f.user_input()
	f.temperature_conversion()

if __name__ == "__main__":
	main()