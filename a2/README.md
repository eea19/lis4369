> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Elka Anistratenko

### Assignment 2 Requirements:

*Three Parts*:

1. Payroll Application
2. Chapter 3 & 4 Questions
3. Link to Bitbucket repo

#### README.md file should include the following items:

* Screenshots of running payroll application
* Link to A2 .ipynb file: [payroll_calculator.ipynb](a2_payroll_calculator/payroll_calculator.ipynb "A2 Jupyter Notebook")
* Screenshots of Python skill sets

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>

#### Assignment Screenshots:

*Screenshots of Payroll Application (VS Code)*:

Payroll No Overtime: | Payroll with Overtime:
--- | ---
![A2 Payroll Calculator No Overtime Screenshot](img/a2_payroll_without_overtime.png) | ![A2 Payroll Calculator With Overtime Screenshot](img/a2_payroll_with_overtime.png)

##### A2 Jupyter Notebook:

![payroll_calculator.ipynb](img/a2_jupyter_notebook_1.png "A2 Jupyter Notebook 1")

![payroll_calculator.ipynb](img/a2_jupyter_notebook_2.png "A2 Jupyter Notebook 2")

*Screenshot of Python Skill Sets*:

##### Skill Set 1: Square Feet to Acres

![Skill Set 1 Square Ft to Acres Screenshot](img/ss1_sq_ft_to_acres.png)

##### Skill Set 2: Miles Per Gallon

![Skill Set 2 MPG Screenshot](img/ss2_miles_per_gal.png)

##### Skill Set 3: IT/ICT Student Percentage

![Skill Set 3 Student Percentage Screenshot](img/ss3_it_ict_student_percentage.png)
