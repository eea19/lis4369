#!/usr/bin/env python3
# Developer: Elka Anistratenko
# Course: LIS4369
# Semester: Fall 2021

import functions as f

def main():
    f.get_requirements()
    f.calculate_payroll()

if __name__ == "__main__":
    main()