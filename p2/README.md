> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Elka Anistratenko

### Project 2 Requirements:

*Three Parts:*

1. Code and run lis4369_p2.R.
2. Screenshots of P2 (4-panel view and 2 graphs) in RStudio.
3. History of R questions.


#### README.md file should include the following items:

* Link to [lis4369_p2.R](lis4369/p2/lis4369_p2/lis4369_p2.R "lis4369_p2.R file")
* Link to [lis4369_p2_output.txt](lis4369/p2/lis4369_p2/lis4369_p2_output.txt "lis4369_p2_output.txt file")
* 4-panel screenshot of lis4369_p2.R running in RStudio.
* Screenshots of 2 plots (qplot(), plot(), and barplot()).


##### 4-Panel Screenshot of lis4369_p2.R in RStudio:

![lis4369_p2.R Screenshot](img/lis4369_p2.png)

plot() graph: | qplot() graph: | barplot() graph:
--- | --- | ---
![P2 Graph 1 Screenshot](img/p2_graph_1.png) | ![P2 Graph 2 Screenshot](img/p2_graph_2.png) | ![P2 Graph 3 Screenshot](img/p2_graph_3.png)