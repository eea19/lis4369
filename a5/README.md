> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Elka Anistratenko

### Assignment 5 Requirements:

*Five Parts:*

1. Complete learn_to_use_R tutorial.
2. Code and run lis4369_a5.R.
3. Screenshots of learn_to_use_R tutorial and A5 in RStudio.
4. Screenshots of skill sets 13, 14, and 15.
5. Chapter 11 & 12 questions.


#### README.md file should include the following items:

* Screenshots of learn_to_use_r.R tutorial in RStudio.
* Screenshots of lis4369_a5.R in RStudio.
* Screenshots of Python Skill Sets.


#### Assignment Screenshots:



*learn_to_use_R Tutorial:*

##### 4-Panel Screenshot of learn_to_use_R Tutorial in RStudio:

![learn_to_use_R Tutorial Screenshot](img/learn_to_use_R.png)

Graph 1: | Graph 2:
--- | ---
![R Tutorial Graph 1 Screenshot](img/R_tutorial_graph_1.png) | ![R Tutorial Graph 2 Screenshot](img/R_tutorial_graph_2.png)



*lis4369_a5.R:*

##### 4-Panel Screenshot of lis4369_a5.R in RStudio:

![lis4369_a5.R Screenshot](img/lis4369_a5.png)

Graph 1: | Graph 2:
--- | ---
![A5 Graph 1 Screenshot](img/a5_graph_1.png) | ![A5 Graph 2 Screenshot](img/a5_graph_2.png)


#### Screenshots of Python Skill Sets:

*Skill Set 13: Sphere Volume Calculator*

![Skill Set 13 Sphere Volume Calculator Screenshot](img/ss13.png)

*Skill Set 14: Python Calculator w/ Error Handling*

![Skill Set 14 Python Calculator w/ Error Handling Screenshot](img/ss14.png)

*Skill Set 15: Python File Read/Write*

![Skill Set 15 Python File Read/Write Screenshot](img/ss15.png)
