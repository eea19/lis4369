> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Elka Anistratenko

### Project 1 Requirements:

*Three Parts*:

1. Data Analysis 1
2. Chapter 7 & 8 Questions
3. Link to Bitbucket repo

#### README.md file should include the following items:

* Screenshots of running painting estimator application
* Link to P1 .ipynb file: [data_analysis_1.ipynb](p1_data_analysis_1/data_analysis_1.ipynb "P1 Jupyter Notebook")
* Screenshots of Python skill sets

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>

#### Project Screenshots:

*Screenshots of Data Analysis 1 (VS Code)*:

Data Analaysis 1 pt.1: | Data Analaysis 1 pt.2:
--- | ---
![P1 Data Analysis 1 Screenshot](img/p1_data_analysis_1.png) | ![P1 Data Analysis 1 Screenshot](img/p1_data_analysis_2.png)

*Screenshots of Data Analysis 1 Graph*:

![P1 Data Analysis 1 Graph Screenshot](img/p1_graph.png "P1 Data Analysis 1 Graph")

##### P1 Jupyter Notebook:

![data_analysis_1.ipynb](img/p1_jupyter_notebook_1.png "P1 Jupyter Notebook 1")

![data_analysis_1.ipynb](img/p1_jupyter_notebook_2.png "P1 Jupyter Notebook 2")

![data_analysis_1.ipynb](img/p1_jupyter_notebook_3.png "P1 Jupyter Notebook 3")

*Screenshot of Python Skill Sets*:

##### Skill Set 7: Python Lists

![Skill Set 7 Python Lists Screenshot](img/ss7_lists.png)

##### Skill Set 8: Python Tuples

![Skill Set 8 Python Tuples Screenshot](img/ss8_tuples.png)

##### Skill Set 9: Python Sets

![Skill Set 9 Python Sets Screenshot](img/ss9_sets.png)