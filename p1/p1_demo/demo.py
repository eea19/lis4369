# Pandas = "Python Data Analysis Library"
# Be sure to: pip install pandas-datareader (pip3 for Python v3.0)

import datetime as dt
import pandas_datareader as pdr # remote data access for pandas
import matplotlib.pyplot as plt
from matplotlib import style

start = dt.datetime(2010, 1, 1)
# end = dt.datetime(2018, 10, 15)
end = dt.datetime.now() # current date and time
# for "end": *must* use Python function for current day/time

# Read data into Pandas DataFrame
# single series
# df = pdr.DataReader("GDP", "fred", start, end)

# multiple series
df = pdr.DataReader(["DJIA", "SP500"], "fred", start, end)

print("\nPrint number of records: ")
print(len(df))

# Why is it important to run the following print statement...
print("\nPrint columns:")
print(df.columns)

print("\nPrint data frame: ")
print(df) # Note: for effeciency, only prints 60--not *all* records

print("\nPrint first five lines: ")
print(df.head(5))
# Note: "Date" is lower than the other columns as it is treated as an index
# statement goes here...

print("\nPrint last five lines: ")
print(df.tail(5))
# statement goes here...

print("\nPrint first 2 lines: ")
print(df.head(2))
# statement goes here...

print("\nPrint last 2 lines: ")
print(df.tail(2))
# statement goes here...

# Research what these styles do!
# style.use('fivethirtyeight')
# compare with...

style.use('ggplot')

df['DJIA'].plot()
df['SP500'].plot()
plt.legend()
plt.show()