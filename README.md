> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Elka Anistratenko

### LIS4369 Requirements:

*Course Assignments*:

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio Code
    - Create "a1_tip_calculator" application
    - Create "a1_tip_calculator" Jupyter Notebook
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git commands 

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create "a2_payroll_calculator" application
    - Create "a2_payroll_calculator" Jupyter Notebook
    - Organize program into two modules (functions.py and main.py)
    - Provide screenshots of application in VS Code and Jupyter Notebook
    - Attach screenshots for skill sets 1-3

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create "a3_painting_estimator" application
    - Create "a3_painting_estimator" Jupyter Notebook
    - Organize program into two modules (functions.py and main.py)
    - Provide screenshots of application in VS Code and Jupyter Notebook
    - Attach screenshots for skill sets 4-6

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Code and run demo.py
    - Test Python Package Installer with "pip freeze" (install necessary packages if missing)
    - Organize program into two modules (functions.py and main.py)
    - Create at least 3 functions within module that are called by the program:
        * main(): calls at least 2 other functions
        * get_requirements(): displays the program requirements
        * data_analysis_2(): displays data
    - Provide screenshots of application in VS Code and Jupyter Notebook
    - Attach screenshots for skill sets 10-12
    
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Complete learn_to_use_R tutorial.
    - Code and run lis4369_a5.R.
    - 4 panel screenshots of tutorial and A5 in RStudio.
    - 4 screenshots of additional graphs:
        * 2 for learn_to_use_R tutorial
        * 2 for lis4369_a5.R
    - Screenshots of skill sets 13-15.

*Course Projects*:

1. [P1 README.md](p1/README.md "My P1 README.md file")
    - Code and run demo.py
    - Test Python Package Installer with "pip freeze" (install necessary packages if missing)
    - Organize program into two modules (functions.py and main.py)
    - Create at least 3 functions within module that are called by the program:
        * main(): calls at least 2 other functions
        * get_requirements(): displays the program requirements
        * data_analysis_1(): displays data
    - Provide screenshots of application in VS Code and Jupyter Notebook
    - Attach screenshots for skill sets 7-9

2. [P2 README.md](p2/README.md "My P2 README.md file")
    - Code and run lis4369_p2.R
    - Include links to lis4369_p2.R and lis4369_p2_output.txt in assignment README.md file.
    - Attach 4-panel screenshot of program running in RStudio.
    - Attach screenshots of graphs: 
        * plot()
        * qplot()
        * barplot()