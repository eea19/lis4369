> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Elka Anistratenko

### Assignment 3 Requirements:

*Three Parts*:

1. Painting Estimator Application
2. Chapter 5 & 6 Questions
3. Link to Bitbucket repo

#### README.md file should include the following items:

* Screenshots of running painting estimator application
* Link to A3 .ipynb file: [painting_estimator.ipynb](a3_painting_estimator/painting_estimator.ipynb "A3 Jupyter Notebook")
* Screenshots of Python skill sets

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>

#### Assignment Screenshots:

*Screenshots of Painting Estimator Application (VS Code)*:

Painting Estimator
![A3 Painting Estimator Screenshot](img/a3_painting_estimator.png)

##### A3 Jupyter Notebook:

![painting_estimator.ipynb](img/a3_jupyter_notebook_1.png "A3 Jupyter Notebook 1")

![painting_estimator.ipynb](img/a3_jupyter_notebook_2.png "A3 Jupyter Notebook 2")

*Screenshot of Python Skill Sets*:

##### Skill Set 4: Calorie Percentage

![Skill Set 4 Calorie Percentage Screenshot](img/ss4_calorie_percentage.png)

##### Skill Set 5: Python Selection Structures

![Skill Set 5 Python Selection Structures Screenshot](img/ss5_selection_structures.png)

##### Skill Set 6: Python Looping Structures

![Skill Set 6 Python Looping Structures Screenshot](img/ss6_loops.png)