> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Elka Anistratenko

### Assignment 4 Requirements:

*Four Parts*:

1. Data Analysis 2
2. Chapter 9 & 10 Questions
3. Link to Bitbucket repo
4. Skill Sets 10-12

#### README.md file should include the following items:

* Screenshots of running data analysis 2 program
* Link to A4 .ipynb file: [data_analysis_2.ipynb](a4_data_analysis_2/data_analysis_2.ipynb "A4 Jupyter Notebook")
* Screenshots of Python skill sets 10-12

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>

#### Project Screenshots:

*Screenshots of Data Analysis 2 (VS Code)*:

Statistical Data Analaysis 2: | Graph of Data Analaysis 2:
--- | ---
![A4 Data Analysis 2 Screenshot](img/a4_data_analysis_2.png "A4 Statistical Data Analysis 2") | ![A4 Data Analysis 2 Graph Screenshot](img/a4_graph.png "A4 Data Analysis 2 Graph")

##### A4 Jupyter Notebook:

![data_analysis_2.ipynb](img/a4_jupyter_notebook_1.png "A4 Jupyter Notebook 1")

![data_analysis_2.ipynb](img/a4_jupyter_notebook_2.png "A4 Jupyter Notebook 2")

![data_analysis_2.ipynb](img/a4_jupyter_notebook_3.png "A4 Jupyter Notebook 3")

![data_analysis_2.ipynb](img/a4_jupyter_notebook_4.png "A4 Jupyter Notebook 4")

![data_analysis_2.ipynb](img/a4_jupyter_notebook_5.png "A4 Jupyter Notebook 5")

![data_analysis_2.ipynb](img/a4_jupyter_notebook_6.png "A4 Jupyter Notebook 6")

*Screenshot of Python Skill Sets*:

##### Skill Set 10: Python Dictionaries

![Skill Set 10 Python Dictionaries Screenshot](img/ss10_dictionaries.png "Skill Set 10 Python Dictionaries")

Skill Set 11: Random Number Generator | Skill Set 12: Temperature Conversion Program
--- | ---
![Skill Set 11 Random Number Generator Screenshot](img/ss11_random_num_generator.png "Skill Set 11 Random Number Generator") | ![Skill Set 12 Temperature Conversion Program Screenshot](img/ss12_temp_conversion_program.png "Skill Set 12 Temperature Conversion Program")